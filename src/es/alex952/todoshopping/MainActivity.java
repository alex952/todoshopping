package es.alex952.todoshopping;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import es.alex952.todoshopping.NewItemDialogFragment.NewItemDialogListener;
import es.alex952.todoshopping.adapters.ShoppingItemCursorAdapter;
import es.alex952.todoshopping.model.ShoppingItem;
import es.alex952.todoshopping.persistence.DBHelper;
import es.alex952.todoshopping.persistence.ShoppingItemDAO;

public class MainActivity extends FragmentActivity implements
		NewItemDialogListener {

	DBHelper mDB;
	ListView itemList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Setting DBHelper
		this.mDB = new DBHelper(this);
		final DBHelper localmDB = this.mDB;

		// Get cursor for all items to display
		ShoppingItemDAO itemDAO = new ShoppingItemDAO();
		itemDAO.setmDB(this.mDB);

		Cursor c = itemDAO.getAll();

		// Setup CursorAdapter for ListView
		ShoppingItemCursorAdapter adapter = new ShoppingItemCursorAdapter(this,
				c);
		adapter.setmDB(this.mDB);

		this.itemList = (ListView) findViewById(R.id.items);
		this.itemList.setAdapter(adapter);

		// ListView's OnClick listener
		this.itemList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				ShoppingItemCursorAdapter adapter = (ShoppingItemCursorAdapter) parent
						.getAdapter();
				Cursor c = (Cursor) adapter.getItem(position);
				int itemId = c.getInt(c.getColumnIndex("_id"));
				ShoppingItemDAO dao = new ShoppingItemDAO();
				dao.setmDB(localmDB);

				ShoppingItem item = dao.get(itemId);

				Intent i = new Intent(MainActivity.this, DetailsActivity.class);
				i.putExtra("data", item);

				MainActivity.this.startActivity(i);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case R.id.add_item:
				NewItemDialogFragment dialog = new NewItemDialogFragment();
				dialog.show(getSupportFragmentManager(),
						"NewItemDialogFragment");

				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onSuccessfulAdd(DialogFragment dialog) {

		EditText itemName = (EditText) dialog.getDialog().findViewById(
				R.id.add_dialog_item_name);
		ShoppingItem newItem = new ShoppingItem(new String(itemName.getText()
				.toString()));
		Spinner cat = (Spinner) dialog.getDialog().findViewById(
				R.id.add_dialog_item_category);

		Cursor c = (Cursor) cat.getSelectedItem();
		int catId;

		ShoppingItemDAO itemsDAO = new ShoppingItemDAO();
		itemsDAO.setmDB(this.mDB);

		if ((catId = c.getInt(c.getColumnIndex("_id"))) >= 0) {
			itemsDAO.create(newItem, catId);
		} else {
			itemsDAO.create(newItem);
		}

		ShoppingItemCursorAdapter adapter = (ShoppingItemCursorAdapter) this.itemList
				.getAdapter();
		Cursor cItems = itemsDAO.getAll();

		adapter.changeCursor(cItems);

		Toast.makeText(this, itemName.getText().toString(), Toast.LENGTH_SHORT)
				.show();

		dialog.dismiss();
	}

	@Override
	public void onCancelledfulAdd(DialogFragment dialog) {
		dialog.dismiss();
	}

}
