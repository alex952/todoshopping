package es.alex952.todoshopping;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import es.alex952.todoshopping.persistence.CategoryDAO;
import es.alex952.todoshopping.persistence.CategoryTable;
import es.alex952.todoshopping.persistence.DBHelper;

public class NewItemDialogFragment extends DialogFragment {

	private View view = null;
	private DBHelper mDB = null;
	
	private String defaultCategory;

	/**
	 * Counter for avoid first fake-triggered selectedItem on Spinner
	 */
	private static int counter;

	/**
	 * Implements notification for the activity that opens the dialog
	 * 
	 * @author alex952
	 * 
	 */
	public interface NewItemDialogListener {
		public void onSuccessfulAdd(DialogFragment dialog);

		public void onCancelledfulAdd(DialogFragment dialog);
	}

	NewItemDialogListener listener;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		NewItemDialogFragment.counter = 0;
		this.defaultCategory = getActivity().getResources().getString(R.string.add_dialog_no_category);

		// Inflate layout
		LayoutInflater inflater = getActivity().getLayoutInflater();

		this.view = inflater.inflate(R.layout.add_new_item, null);

		// Setting properties, layout, adapter, and handlers of spinner
		Spinner cat = (Spinner) view
				.findViewById(R.id.add_dialog_item_category);
		cat.setPrompt(getResources().getString(
				R.string.add_dialog_category_prompt));

		this.mDB = new DBHelper(this.getActivity());
		CategoryDAO catDAO = new CategoryDAO();
		catDAO.setmDB(this.mDB);

		@SuppressWarnings("deprecation")
		SimpleCursorAdapter catAdapter = new SimpleCursorAdapter(
				this.getActivity(),
				android.R.layout.simple_spinner_item,
				catDAO.getWithQuery(
						"select -1 AS _id, ? AS name UNION SELECT _id, name from category UNION select -2 AS _id, ? AS name",
						new String[] {
								getResources().getString(
										R.string.add_dialog_no_category),
								getResources().getString(
										R.string.add_dialog_add_category) }),
				new String[] { "name" }, new int[] { android.R.id.text1 });

		cat.setAdapter(catAdapter);
		catAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		this.setSpinnerDefaultPosition();

		cat.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				if (NewItemDialogFragment.counter > 0) {
					Cursor c = (Cursor) parent.getAdapter().getItem(position);
					int x = c.getInt(c.getColumnIndex("_id"));

					if (x == -1) {

					} else if (x == -2) {

					}
				} else {
					NewItemDialogFragment.counter++;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				return;
			}
		});

		// Dialog builder
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder.setView(view)
				.setPositiveButton(R.string.add_dialog_add_button_text,
						new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								listener.onSuccessfulAdd(NewItemDialogFragment.this);
							}
						})
				.setNegativeButton(R.string.add_dialog_cancel_button_text,
						new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								listener.onCancelledfulAdd(NewItemDialogFragment.this);
							}
						});

		return builder.create();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			listener = (NewItemDialogListener) activity;
		} catch (ClassCastException ex) {
			throw new ClassCastException("Class " + activity.toString()
					+ " must implement the interface NewItemDialogListener");
		}
	}

	private void setSpinnerDefaultPosition() {
		setSelecteItemSpinnerOnItem(this.defaultCategory);
	}

	private void setSelecteItemSpinnerOnItem(String itemString) {
		Spinner sp = (Spinner) this.view
				.findViewById(R.id.add_dialog_item_category);
		SimpleCursorAdapter catAdapter = (SimpleCursorAdapter) sp.getAdapter();

		Cursor c = catAdapter.getCursor();

		int defaultPosition = -1;
		c.moveToFirst();

		do {
			if (c.getString(c.getColumnIndex(CategoryTable.COLUMN_NAME_NAME))
					.equals(itemString)) {
				defaultPosition = c.getPosition();
			}
		} while (c.moveToNext() && defaultPosition == -1);

		sp.setSelection(defaultPosition);
	}
}
