package es.alex952.todoshopping.persistence;

import android.content.res.Resources.NotFoundException;
import android.database.Cursor;

public interface DAOInterface<T> {
	
	public void create(T obj);
	public T get(Integer id) throws NotFoundException;
	public void delete(int id);
	public void delete(T obj);
	public void update(T obj);
	public Cursor getAll();
	
	public Cursor getWithQuery(String query, String[] args);
}
