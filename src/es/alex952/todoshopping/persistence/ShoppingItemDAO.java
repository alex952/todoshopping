package es.alex952.todoshopping.persistence;

import es.alex952.todoshopping.model.ShoppingItem;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.res.Resources.NotFoundException;

public class ShoppingItemDAO implements DAOInterface<ShoppingItem> {

	DBHelper mDB = null;

	public void setmDB(DBHelper mDB) {
		this.mDB = mDB;
	}

	@Override
	public void create(ShoppingItem obj) {
		if (this.mDB == null)
			return;

		SQLiteDatabase db = this.mDB.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(ShoppingItemTable.COLUMN_NAME_NAME, obj.getName());
		values.put(ShoppingItemTable.COLUMN_NAME_CHECKED, (obj.isChecked()) ? 1
				: 0);
		values.putNull(ShoppingItemTable.COLUMN_NAME_CATEGORY);

		db.insert(ShoppingItemTable.TABLE_NAME, null, values);
		db.close();
	}

	public void create(ShoppingItem obj, int categoryId) {
		if (this.mDB == null)
			return;

		SQLiteDatabase db = this.mDB.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(ShoppingItemTable.COLUMN_NAME_NAME, obj.getName());
		values.put(ShoppingItemTable.COLUMN_NAME_CHECKED, (obj.isChecked()) ? 1
				: 0);
		values.put(ShoppingItemTable.COLUMN_NAME_CATEGORY, categoryId);

		db.insert(ShoppingItemTable.TABLE_NAME, null, values);
		db.close();
	}

	@Override
	public ShoppingItem get(Integer id) {
		SQLiteDatabase db = this.mDB.getReadableDatabase();

		Cursor c = db.query(ShoppingItemTable.TABLE_NAME, new String[] {
				ShoppingItemTable._ID, ShoppingItemTable.COLUMN_NAME_NAME,
				ShoppingItemTable.COLUMN_NAME_CHECKED,
				ShoppingItemTable.COLUMN_NAME_CATEGORY }, ShoppingItemTable._ID
				+ " = ?", new String[] { id.toString() }, null, null, null,
				null);

		if (c == null)
			throw new NotFoundException();
		else if (c.getCount() == 0)
			throw new NotFoundException();

		c.moveToFirst();
		ShoppingItem it = new ShoppingItem(c.getInt(c
				.getColumnIndex(ShoppingItemTable._ID)), c.getString(c
				.getColumnIndex(ShoppingItemTable.COLUMN_NAME_NAME)));
		it.setChecked(c.getInt(c
				.getColumnIndex(ShoppingItemTable.COLUMN_NAME_CHECKED)) == 1);

		try {
			int cat = c.getInt(c
					.getColumnIndex(ShoppingItemTable.COLUMN_NAME_CATEGORY));
			CategoryDAO catDAO = new CategoryDAO();
			catDAO.setmDB(this.mDB);

			it.setCategory(catDAO.get(cat));
		} catch (Exception ex) {
			it.setCategory(null);
		}

		return it;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(ShoppingItem obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(ShoppingItem obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public Cursor getAll() {
		if (mDB == null)
			return null;

		SQLiteDatabase db = mDB.getReadableDatabase();
		Cursor c = db.query(ShoppingItemTable.TABLE_NAME, new String[] {
				ShoppingItemTable._ID, ShoppingItemTable.COLUMN_NAME_NAME,
				ShoppingItemTable.COLUMN_NAME_CATEGORY,
				ShoppingItemTable.COLUMN_NAME_CHECKED }, null, null, null,
				null, ShoppingItemTable._ID + " ASC");

		return c;
	}

	@Override
	public Cursor getWithQuery(String query, String[] args) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Custom method to change the checked state of the item
	 * 
	 * @param id
	 *            ID of the item to be changed
	 * @param state
	 *            New state
	 */
	public void updateState(Integer id, boolean state) {
		SQLiteDatabase db = this.mDB.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ShoppingItemTable.COLUMN_NAME_CHECKED, state ? 1 : 0);

		db.update(ShoppingItemTable.TABLE_NAME, values, " _id = ?",
				new String[] { id.toString() });
	}
}
