package es.alex952.todoshopping.persistence;

import es.alex952.todoshopping.model.CategoryItem;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.res.Resources.NotFoundException;

public class CategoryDAO implements DAOInterface<CategoryItem> {

	DBHelper mDB = null;

	public void setmDB(DBHelper mDB) {
		this.mDB = mDB;
	}

	@Override
	public void create(CategoryItem obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public CategoryItem get(Integer id) {
		SQLiteDatabase db = this.mDB.getReadableDatabase();

		Cursor c = db.query(CategoryTable.TABLE_NAME, new String[] {
				CategoryTable._ID, CategoryTable.COLUMN_NAME_NAME, },
				CategoryTable._ID + " = ?", new String[] { id.toString() },
				null, null, null, null);

		if (c == null || c.getCount() == 0)
			throw new NotFoundException();

		CategoryItem cat = new CategoryItem(c.getInt(c
				.getColumnIndex(CategoryTable._ID)), c.getString(c
				.getColumnIndex(CategoryTable.COLUMN_NAME_NAME)));

		return cat;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(CategoryItem obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(CategoryItem obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public Cursor getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cursor getWithQuery(String query, String[] args) {

		SQLiteDatabase db = this.mDB.getReadableDatabase();
		Cursor c = db.rawQuery(query, args);

		return c;
	}

}
