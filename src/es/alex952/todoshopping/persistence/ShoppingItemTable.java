package es.alex952.todoshopping.persistence;

import android.provider.BaseColumns;

public class ShoppingItemTable implements BaseColumns {

	public static final String TABLE_NAME = "shopping_item";
	public static final String COLUMN_NAME_NAME = "name";
	public static final String COLUMN_NAME_CHECKED = "checked";
	public static final String COLUMN_NAME_CATEGORY = "category";

	public static final String CREATE_STATEMENT = "CREATE TABLE " + TABLE_NAME
			+ " (" + _ID + " INTEGER PRIMARY KEY," + COLUMN_NAME_NAME
			+ " TEXT NOT NULL," + COLUMN_NAME_CHECKED + " INTEGER NOT NULL,"
			+ COLUMN_NAME_CATEGORY + " INTEGER," + "FOREIGN KEY("
			+ COLUMN_NAME_CATEGORY + ") REFERENCES category(_id))";
	
	public static final String DELETE_STATEMENT = "DROP TABLE IF EXISTS "  + TABLE_NAME;

}
