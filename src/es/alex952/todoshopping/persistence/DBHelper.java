package es.alex952.todoshopping.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "ShoppingList.db";
	
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
	@Override
	public void onCreate(SQLiteDatabase db) {
		 db.execSQL(CategoryTable.CREATE_STATEMENT);
		 db.execSQL(ShoppingItemTable.CREATE_STATEMENT);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		db.execSQL(ShoppingItemTable.DELETE_STATEMENT);
		db.execSQL(CategoryTable.DELETE_STATEMENT);
		onCreate(db);
	}
	
	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}

}
