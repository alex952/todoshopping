package es.alex952.todoshopping.persistence;

import android.provider.BaseColumns;

public class CategoryTable implements BaseColumns {

	public static final String TABLE_NAME = "category";
	public static final String COLUMN_NAME_NAME = "name";

	public static final String CREATE_STATEMENT = "CREATE TABLE " + TABLE_NAME
			+ " (" + _ID + " INTEGER PRIMARY KEY," + COLUMN_NAME_NAME
			+ " TEXT UNIQUE)";

	public static final String DELETE_STATEMENT = "DROP TABLE IF EXISTS "
			+ TABLE_NAME;

}
