package es.alex952.todoshopping;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import es.alex952.todoshopping.model.ShoppingItem;

public class DetailsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent i = getIntent();
		ShoppingItem data = (ShoppingItem) i.getParcelableExtra("data");

		TextView text_name = (TextView) findViewById(R.id.details_text_name);
		text_name.setText(data.getName());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				Intent parentIntent = new Intent(this, MainActivity.class);
				parentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);

				startActivity(parentIntent);
				finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}

	}

}
