package es.alex952.todoshopping.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CategoryItem implements Parcelable {

	private Integer id = null;
	private String name;
	
	public CategoryItem(Integer id, String name) {
		this(name);
		this.id = id;
	}
	
	public CategoryItem(String name) {
		this.name = name;
	}
	
	public CategoryItem(Parcel in) {
		readFromParcel(in);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
	}
	
	private void readFromParcel(Parcel origin) {
		this.name = origin.readString();
	}
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public CategoryItem createFromParcel(Parcel in) {
			return new CategoryItem(in);
		}

		public CategoryItem[] newArray(int size) {
			return new CategoryItem[size];
		}
	};
	
	public String toString() {
		return getName();
	};
}
