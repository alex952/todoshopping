package es.alex952.todoshopping.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ShoppingItem implements Parcelable {

	private Integer id = null;
	private String name;
	private Boolean checked;
	private CategoryItem category = null;
	
	public ShoppingItem(String name) {
		this.name = name;
		this.checked = false;
	}
	
	public ShoppingItem(Integer id, String name) {
		this(name);
		this.id = id;
	}
	
	public ShoppingItem(String name, CategoryItem category) {
		this(name);
		this.category = category;
	}

	public ShoppingItem(Parcel in) {
		readFromParcel(in);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Boolean isChecked() {
		return checked;
	}

	public void toggleChecked() {
		this.checked = (this.checked) ? false : true;
	}
	
	public void setChecked(boolean state) {
		this.checked = state;
	}

	public CategoryItem getCategory() {
		return category;
	}

	public void setCategory(CategoryItem category) {
		this.category = category;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeByte((byte) (this.checked ? 1 : 0));
		dest.writeParcelable(category, flags);
	}

	private void readFromParcel(Parcel origin) {
		this.name = origin.readString();
		this.checked = origin.readByte() == 1;
		this.category = origin.readParcelable(CategoryItem.class.getClassLoader());
	}

	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public ShoppingItem createFromParcel(Parcel in) {
			return new ShoppingItem(in);
		}

		public ShoppingItem[] newArray(int size) {
			return new ShoppingItem[size];
		}
	};
}
