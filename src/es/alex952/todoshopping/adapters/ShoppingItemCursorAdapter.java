package es.alex952.todoshopping.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import es.alex952.todoshopping.R;
import es.alex952.todoshopping.persistence.DBHelper;
import es.alex952.todoshopping.persistence.ShoppingItemDAO;
import es.alex952.todoshopping.persistence.ShoppingItemTable;

public class ShoppingItemCursorAdapter extends ResourceCursorAdapter {

	private DBHelper mDB = null;

	public void setmDB(DBHelper mDB) {
		this.mDB = mDB;
	}

	public ShoppingItemCursorAdapter(Context context, Cursor c) {
		super(context, R.layout.listview_shopping_item, c, 0);
	}

	@Override
	public void bindView(View view, final Context context, Cursor c) {
		TextView name = (TextView) view.findViewById(R.id.itemName);
		CheckBox checked = (CheckBox) view.findViewById(R.id.itemChecked);
		
		name.setText(c.getString(c.getColumnIndex(ShoppingItemTable.COLUMN_NAME_NAME)));
		checked.setChecked(c.getInt(c.getColumnIndex(ShoppingItemTable.COLUMN_NAME_CHECKED)) == 1);
		int id = c.getInt(c.getColumnIndex("_id"));
		checked.setTag(id);
		
		
		checked.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				int id = (Integer)buttonView.getTag();
				
				ShoppingItemDAO itemsDAO = new ShoppingItemDAO();
				itemsDAO.setmDB(mDB);
				
				itemsDAO.updateState(id, isChecked);
			}
		});
	}
}
