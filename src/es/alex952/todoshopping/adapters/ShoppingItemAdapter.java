package es.alex952.todoshopping.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import es.alex952.todoshopping.DetailsActivity;
import es.alex952.todoshopping.R;
import es.alex952.todoshopping.model.ShoppingItem;

public class ShoppingItemAdapter extends ArrayAdapter<ShoppingItem> {

	private int resource;
	private Context context;
	List<ShoppingItem> items;
	
	public ShoppingItemAdapter(Context context, int resource, 
			List<ShoppingItem> objects) {
		super(context, resource, objects);
		
		this.resource = resource;
		this.context = context;
		this.items = objects;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		
		View row = convertView;
		ShoppinItemHolder holder = null;
		
		
		if (row == null) {
			LayoutInflater inflater = ((Activity)this.context).getLayoutInflater();
            row = inflater.inflate(this.resource, parent, false);
            
            holder = new ShoppinItemHolder();
            holder.itemChecked = (CheckBox) row.findViewById(R.id.itemChecked);
            holder.itemName = (TextView) row.findViewById(R.id.itemName);
            
            row.setTag(holder);
		} else {
			holder = (ShoppinItemHolder) row.getTag();
		}
		
		final ShoppingItem item = this.items.get(position);
		holder.itemName.setText(item.getName());
		holder.itemChecked.setChecked(item.isChecked());
		
		holder.itemChecked.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				item.toggleChecked();
				
				Toast.makeText(parent.getContext(), item.isChecked() + ":" + isChecked, Toast.LENGTH_SHORT).show();
			}
		});
		
		holder.itemName.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getContext(), DetailsActivity.class);
				i.putExtra("data", item);
				
				getContext().startActivity(i);
			}
		});
		
		return row;
	}
	
	static class ShoppinItemHolder {
		CheckBox itemChecked;
		TextView itemName;
	}
}
